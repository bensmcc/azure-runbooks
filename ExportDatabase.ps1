workflow ExportDatabase
{
    Param(
        [Parameter(Mandatory = $true)]
        [string]$DatabaseName,
        [Parameter(Mandatory = $true)]
        [string]$ServerName,
        [Parameter(Mandatory = $true)]
        [string]$ServerAdmin,
        [Parameter(Mandatory = $true)]
        [string]$ResourceGroupName,
        [Parameter(Mandatory = $true)]
        [string]$ServerPassword,
        [Parameter(Mandatory = $true)]
        [string]$BaseStorageUri,
        [Parameter(Mandatory = $true)]
        [string]$StorageKey
    )
    inlineScript
    {
        $connectionName = "AzureRunAsConnection"
        try {
            # Get the connection "AzureRunAsConnection "
            $servicePrincipalConnection = Get-AutomationConnection -Name $connectionName

            "Login to Azure"
            Add-AzureRmAccount `
                -ServicePrincipal `
                -TenantId $servicePrincipalConnection.TenantId `
                -ApplicationId $servicePrincipalConnection.ApplicationId `
                -CertificateThumbprint $servicePrincipalConnection.CertificateThumbprint
        }
        catch {
            if (!$servicePrincipalConnection) {
                $ErrorMessage = "Connection $connectionName not found."
                throw $ErrorMessage
            }
            else {
                Write-Error -Message $_.Exception
                throw $_.Exception
            }
        }

        $securePassword = ConvertTo-SecureString -String $USING:ServerPassword -AsPlainText -Force
        $creds = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $USING:ServerAdmin, $securePassword

        $bacpacFilename = $USING:DatabaseName + ".bacpac"

        $BacpacUri = $USING:BaseStorageUri + "/" + $bacpacFilename
        $StorageKeytype = "StorageAccessKey"

        $exportRequest = New-AzureRmSqlDatabaseExport -ResourceGroupName $USING:ResourceGroupName -ServerName $USING:ServerName `
            -DatabaseName $USING:DatabaseName -StorageKeytype $StorageKeytype -StorageKey $USING:StorageKey -StorageUri $BacpacUri `
            -AdministratorLogin $creds.UserName -AdministratorLoginPassword $creds.Password

        $operationStatusLink = $exportRequest.OperationStatusLink

        $exportStatus = Get-AzureRmSqlDatabaseImportExportStatus -OperationStatusLink $operationStatusLink

        while($exportStatus.Status -eq "InProgress")
        {
            "Status: $($exportStatus.Status) -- StatusMessage: $($exportStatus.StatusMessage)"

            Start-Sleep -s 5

            $exportStatus = Get-AzureRmSqlDatabaseImportExportStatus -OperationStatusLink $operationStatusLink
        }

        "FINAL - Status: $($exportStatus.Status) -- StatusMessage: $($exportStatus.StatusMessage)"

        "Azure SQL DB Export Completed?"
    }
}