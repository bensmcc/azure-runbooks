## This repo contains Azure Runbooks that I've found useful for automating tasks

- CopyDatabase.ps1 - Will initialize a database copy
- DeleteDatabase.ps1 - Deletes a database
- ExecuteQuery.ps1 - Runs a SQL query against a database
- ExportDatabase.ps1 - Initializes a database export in Azure and waits until it has finished

These have been used in conjunction with Azure Logic Apps to create a copy of a database, run sql against it, export it, then finally delete the database. Something that used to take up developers' time now is automated and happens a few times per week.