workflow CopyDatabase {
    Param(
        [Parameter(Mandatory = $true)]
        [string] $sourceResourceGroup,
        [Parameter(Mandatory = $true)]
        [string] $sourceDatabaseServer,
        [Parameter(Mandatory = $true)]
        [string] $sourceDatabase,

        [Parameter(Mandatory = $true)]
        [string] $destResourceGroup,
        [Parameter(Mandatory = $true)]
        [string] $destDatabaseServer,
        [Parameter(Mandatory = $true)]
        [string] $destDatabase,
        [Parameter(Mandatory = $false)]
        [string] $destElasticPool
    )
    inlineScript {
        $connectionName = "AzureRunAsConnection"
        try {
            # Get the connection "AzureRunAsConnection "
            $servicePrincipalConnection = Get-AutomationConnection -Name $connectionName

            # "Login to Azure"
            Add-AzureRmAccount `
                -ServicePrincipal `
                -TenantId $servicePrincipalConnection.TenantId `
                -ApplicationId $servicePrincipalConnection.ApplicationId `
                -CertificateThumbprint $servicePrincipalConnection.CertificateThumbprint
        }
        catch {
            if (!$servicePrincipalConnection) {
                $ErrorMessage = "Connection $connectionName not found."
                throw $ErrorMessage
            }
            else {
                Write-Error -Message $_.Exception
                throw $_.Exception
            }
        }

        function CopyDatabaseWithoutElasticPool([string] $sourceResourceGroup, [string] $sourceDatabaseServer, [string] $sourceDatabaseName, [string] $destResourceGroup, [string] $destDatabaseServer, [string] $destDatabaseName)
        {
            New-AzureRmSqlDatabaseCopy `
                -ResourceGroupName $sourceResourceGroup `
                -ServerName $sourceDatabaseServer `
                -DatabaseName $sourceDatabaseName `
                -CopyResourceGroupName $destResourceGroup `
                -CopyServerName $destDatabaseServer `
                -CopyDatabaseName $destDatabaseName
        }

        function CopyDatabaseWithElasticPool([string] $sourceResourceGroup, [string] $sourceDatabaseServer, [string] $sourceDatabaseName, [string] $destResourceGroup, [string] $destDatabaseServer, [string] $destDatabaseName, [string] $destElasticPool)
        {
            New-AzureRmSqlDatabaseCopy `
                -ResourceGroupName $sourceResourceGroup `
                -ServerName $sourceDatabaseServer `
                -DatabaseName $sourceDatabaseName `
                -CopyResourceGroupName $destResourceGroup `
                -CopyServerName $destDatabaseServer `
                -CopyDatabaseName $destDatabaseName `
                -ElasticPoolName $destElasticPool
        }

        if ($USING:destElasticPool -eq $null -Or $USING:destElasticPool -eq "null" -Or [string]::IsNullOrEmpty($USING:destElasticPool))
        {
            Write-Host "running without pool";

            CopyDatabaseWithoutElasticPool `
                $USING:sourceResourceGroup `
                $USING:sourceDatabaseServer `
                $USING:sourceDatabase `
                $USING:destResourceGroup `
                $USING:destDatabaseServer `
                $USING:destDatabase
        }
        else
        {
            Write-Host "running with pool";

            CopyDatabaseWithElasticPool `
                $USING:sourceResourceGroup `
                $USING:sourceDatabaseServer `
                $USING:sourceDatabase `
                $USING:destResourceGroup `
                $USING:destDatabaseServer `
                $USING:destDatabase `
                $USING:destElasticPool
        }
    }
}