workflow DeleteDatabase
{
    Param(
        [Parameter(Mandatory = $true)]
        [string]$DatabaseName,
        [Parameter(Mandatory = $true)]
        [string]$ServerName,
        [Parameter(Mandatory = $true)]
        [string]$ResourceGroupName
    )
    inlineScript
    {
        $connectionName = "AzureRunAsConnection"
        try {
            # Get the connection "AzureRunAsConnection "
            $servicePrincipalConnection = Get-AutomationConnection -Name $connectionName

            "Login to Azure"
            Add-AzureRmAccount `
                -ServicePrincipal `
                -TenantId $servicePrincipalConnection.TenantId `
                -ApplicationId $servicePrincipalConnection.ApplicationId `
                -CertificateThumbprint $servicePrincipalConnection.CertificateThumbprint
        }
        catch {
            if (!$servicePrincipalConnection) {
                $ErrorMessage = "Connection $connectionName not found."
                throw $ErrorMessage
            }
            else {
                Write-Error -Message $_.Exception
                throw $_.Exception
            }
        }

        Remove-AzureRmSqlDatabase -DatabaseName $USING:DatabaseName -ServerName $USING:ServerName -ResourceGroupName $USING:ResourceGroupName
    }
}