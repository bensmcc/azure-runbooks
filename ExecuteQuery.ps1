workflow ExecuteQuery
{
    Param(
        [Parameter(Mandatory = $true)]
        [string]$DatabaseServer,
        [Parameter(Mandatory = $true)]
        [string]$DatabaseName,
        [Parameter(Mandatory = $true)]
        [string]$ServerAdmin,
        [Parameter(Mandatory = $true)]
        [string]$ServerPassword,
        [Parameter(Mandatory = $true)]
        [string]$Query,
        [Parameter(Mandatory = $true)]
        [int]$Timeout
    )
    inlinescript
    {
        Write-Output "JOB START"
        # Create connection to Master DB
        $MasterDatabaseConnection = New-Object System.Data.SqlClient.SqlConnection
        $MasterDatabaseConnection.ConnectionString = "Data Source=" + $USING:DatabaseServer + ".database.windows.net;Initial Catalog=" + $USING:DatabaseName + ";Integrated Security=False;User ID=" + $USING:ServerAdmin + ";Password=" + $USING:ServerPassword + ";Connect Timeout=60;Encrypt=False;TrustServerCertificate=False"
        $MasterDatabaseConnection.Open()

        Write-Output "CONNECTION OPEN"

        # Create command
        $MasterDatabaseCommand = New-Object System.Data.SqlClient.SqlCommand
        $MasterDatabaseCommand.CommandTimeout = $USING:Timeout;
        $MasterDatabaseCommand.Connection = $MasterDatabaseConnection
        $MasterDatabaseCommand.CommandText = $USING:Query

        Write-Output "DATABASE COMMAND TEXT ASSIGNED"

        # Execute the query
        $MasterDatabaseCommand.ExecuteNonQuery()

        Write-Output "EXECUTING QUERY"

        # Close connection to Master DB
        $MasterDatabaseConnection.Close()

        Write-Output "CONNECTION CLOSED"
    }
}